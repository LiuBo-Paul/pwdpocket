//
//  AboutPwdPocketViewController.m
//  PwdPocket
//
//  Created by Paul on 2019/6/19.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import "AboutPwdPocketViewController.h"

@interface AboutPwdPocketViewController ()

@property (strong, nonatomic) IBOutlet UILabel *msgLabel;
@property (strong, nonatomic) IBOutlet UILabel *moreMsgLabel;

@end
static NSString *AppId = @"1469018173";

@implementation AboutPwdPocketViewController

static NSString *aboutPwdPocketStr = @"http://18.221.81.60/AboutPwdPocket.txt";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.msgLabel.text = @"小口袋    2019年06月18日    降生了\n^ 0 ^";
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *content = [NSString stringWithContentsOfURL:[NSURL URLWithString:aboutPwdPocketStr] encoding:(NSUTF8StringEncoding) error:nil];
            if(content.length > 0) {
                strongSelf.moreMsgLabel.text = content;
            } else {
                strongSelf.moreMsgLabel.text = @"暂无内容";
            }
        });
    });
}

- (IBAction)backBtnAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)recommendBtnAction:(UIButton *)sender {
    //跳转到评论页面 添加评论
    NSString *itunesurl = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/cn/app/id%@?mt=8&action=write-review", AppId];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:itunesurl] options:@{} completionHandler:nil];
}

@end
