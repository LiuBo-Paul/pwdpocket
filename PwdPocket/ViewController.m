//
//  ViewController.m
//  PwdPocket
//
//  Created by Paul on 2019/5/13.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic) IBOutlet UISwitch *inOrOutSwitch;
@property (strong, nonatomic) IBOutlet UITextField *platformTextField;
@property (strong, nonatomic) IBOutlet UITextField *accountTextField;
@property (strong, nonatomic) IBOutlet UITextField *pwdTextField;
@property (strong, nonatomic) IBOutlet UIButton *sureBtn;
@property (assign, nonatomic) BOOL isLoacalAuthentication;
@property (strong, nonatomic) IBOutlet UILabel *stateLabel;
@property (strong, nonatomic) IBOutlet UITableView *keysNameTableView;
@property (strong, nonatomic) NSMutableArray *keysNameArray;
@property (strong, nonatomic) IBOutlet UIButton *createPwdBtn;
@property (strong, nonatomic) IBOutlet UIButton *pwdLengthBtn;
@property (strong, nonatomic) NSMutableDictionary *pwdDic;
@property (assign, nonatomic) NSUInteger pwdLength;
@property (assign, nonatomic) PPT_PwdLevel pwdLevel;
@property (strong, nonatomic) IBOutlet UILabel *searchStateLabel;

@end

@implementation ViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[PPT_Manager sharedInstance] readKeysFromKeychain];
    [self refreshKeysTableView];
    [self clearAllTextFieldContent];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    if(!self.isLoacalAuthentication) {
        [self checkLocalAuthentication:^(BOOL isSuccess) {

        }];
    }
}

-(void)checkLocalAuthentication:(LocalAuthenticationManagerIsSuccessBlock)isSuccessBlock {
    __weak typeof(self) weakSelf = self;
    [self hideViews];
    [[PPT_LocalAuthenticationManager sharedInstance] checkLocalAuthenticationStateIsSuccess:^(BOOL isSuccess) {
        [weakSelf showViews];
        if(isSuccess) {
            weakSelf.isLoacalAuthentication = YES;
            [weakSelf showViews];
        } else {
            weakSelf.isLoacalAuthentication = NO;
            [weakSelf hideViews];
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        dispatch_async(dispatch_get_main_queue(), ^{
            [strongSelf.keysNameTableView reloadData];
        });
        isSuccessBlock(isSuccess);
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pwdLength = 6;
    self.pwdLevel = PPT_PwdLevelDefault;
    [self.inOrOutSwitch setOn:YES];
    
    self.sureBtn.clipsToBounds = YES;
    self.sureBtn.layer.cornerRadius = 10;
    self.sureBtn.layer.borderColor = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1].CGColor;
    self.sureBtn.layer.borderWidth = 1.0;
    
    [self changeSwitch:self.inOrOutSwitch];
    self.keysNameTableView.delegate = self;
    self.keysNameTableView.dataSource = self;
    self.keysNameTableView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
    self.keysNameTableView.separatorColor = [UIColor whiteColor];
    [self refreshKeysTableView];
    [self refreshStateLabel:@"刷新完成！"];
    [self checkLocalAuthentication:^(BOOL isSuccess) {
        
    }];
}

-(void)refreshKeysTableView {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.keysNameArray = [NSMutableArray arrayWithArray:[[PPT_Manager sharedInstance] getKeysFromKeychain]];
        [weakSelf.keysNameTableView reloadData];
    });
}

- (IBAction)aboutPwdPocketBtnAction:(UIButton *)sender {
    AboutPwdPocketViewController *aboutPwdPocketViewController = [[AboutPwdPocketViewController alloc] initWithNibName:@"AboutPwdPocketViewController" bundle:[NSBundle mainBundle]];
    aboutPwdPocketViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:aboutPwdPocketViewController animated:YES completion:nil];
}

- (IBAction)settingBtnAction:(UIButton *)sender {
    SettingPwdPocketViewController *settingPwdPocketViewController = [[SettingPwdPocketViewController alloc] initWithNibName:@"SettingPwdPocketViewController" bundle:[NSBundle mainBundle]];
    settingPwdPocketViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:settingPwdPocketViewController animated:YES completion:nil];
}

- (IBAction)inOrOutSwitchChanged:(UISwitch *)sender {
    [self changeSwitch:sender];
}

-(void)changeSwitch:(UISwitch *)inOrOutSwitch {
    __weak typeof(self) weakSelf = self;
    [self clearAllTextFieldContent];
    if(inOrOutSwitch.isOn) {
        [UIView animateWithDuration:0.5 animations:^{
            weakSelf.pwdTextField.alpha = 1.0;
            weakSelf.pwdLengthBtn.alpha = 1.0;
            weakSelf.createPwdBtn.alpha = 1.0;
            [weakSelf.sureBtn setTitle:@"新建" forState:(UIControlStateNormal)];
            weakSelf.searchStateLabel.alpha = 0;
        }];
    } else {
        [UIView animateWithDuration:0.5 animations:^{
            weakSelf.pwdTextField.alpha = 0;
            weakSelf.pwdLengthBtn.alpha = 0;
            weakSelf.createPwdBtn.alpha = 0;
            [weakSelf.sureBtn setTitle:@"查询" forState:(UIControlStateNormal)];
            weakSelf.searchStateLabel.alpha = 1;
        }];
    }
}

- (IBAction)pwdLengthBtnAction:(UIButton *)sender {
    __weak typeof(self) weakSelf = self;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"密码位数" message:@"请输入要生成的密码位数" preferredStyle:(UIAlertControllerStyleAlert)];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"请输入密码位数";
        textField.textAlignment = NSTextAlignmentCenter;
        textField.keyboardType = UIKeyboardTypeNumberPad;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        UITextField *textField = alertController.textFields.firstObject;
        if(textField.text.length == 0) {
            [weakSelf refreshStateLabel:@"请输入密码位数"];
            return;
        }
        if(textField.text.length > 20) {
            [weakSelf refreshStateLabel:@"密码位数过长！"];
            return;
        }
        weakSelf.pwdLength = [textField.text integerValue];
        [weakSelf.pwdLengthBtn setTitle:[NSString stringWithFormat:@"%@位", textField.text] forState:(UIControlStateNormal)];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}

- (IBAction)createPwdBtnAction:(UIButton *)sender {
    __weak typeof(self) weakSelf = self;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"密码复杂度" message:@"请选择密码复杂度" preferredStyle:(UIAlertControllerStyleAlert)];
    [alertController addAction:[UIAlertAction actionWithTitle:@"默认" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        weakSelf.pwdLevel = PPT_PwdLevelDefault;
        weakSelf.pwdTextField.text = [[PPT_Manager sharedInstance] createPwdWithLevel:weakSelf.pwdLevel length:weakSelf.pwdLength];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"低复杂度" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        weakSelf.pwdLevel = PPT_PwdLevelLow;
        weakSelf.pwdTextField.text = [[PPT_Manager sharedInstance] createPwdWithLevel:weakSelf.pwdLevel length:weakSelf.pwdLength];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"高复杂度" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        weakSelf.pwdLevel = PPT_PwdLevelHigh;
        weakSelf.pwdTextField.text = [[PPT_Manager sharedInstance] createPwdWithLevel:weakSelf.pwdLevel length:weakSelf.pwdLength];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}

- (IBAction)sureBtnAction:(UIButton *)sender {
    [self refreshStateLabel:@""];
    __weak typeof(self) weakSelf = self;
    if(self.isLoacalAuthentication) {
        if(self.inOrOutSwitch.isOn) {
            if(self.platformTextField.text.length == 0) {
                [self.platformTextField becomeFirstResponder];
                [self refreshStateLabel:@"请输入平台名"];
                return;
            }
            if(self.accountTextField.text.length == 0) {
                [self.accountTextField becomeFirstResponder];
                [self refreshStateLabel:@"请输入账户名"];
                return;
            }
            if(self.pwdTextField.text.length == 0) {
                [self.pwdTextField becomeFirstResponder];
                [self refreshStateLabel:@"请输入密码"];
                return;
            }
            [[PPT_Manager sharedInstance] createPwdWithAccount:self.accountTextField.text pwd:self.pwdTextField.text platform:self.platformTextField.text isSuccessBlock:^(BOOL isSuccess, NSString * _Nullable msg) {
                __strong typeof(weakSelf) strongSelf = self;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [strongSelf refreshStateLabel:msg];
                    [strongSelf clearAllTextFieldContent];
                    [strongSelf refreshKeysTableView];
                });
            }];
        } else {
            if(self.platformTextField.text.length == 0) {
                [self.platformTextField becomeFirstResponder];
                [self refreshStateLabel:@"请输入平台名"];
                return;
            }
            if(self.accountTextField.text.length == 0) {
                [self.accountTextField becomeFirstResponder];
                [self refreshStateLabel:@"请输入账户名"];
                return;
            }
            [[PPT_Manager sharedInstance] getPwdWithAccount:self.accountTextField.text platform:self.platformTextField.text successBlock:^(NSDictionary * _Nullable dic) {
                [weakSelf checkLocalAuthentication:^(BOOL isSuccess) {
                    if(isSuccess) {
                        [[UIPasteboard generalPasteboard] setString:[NSString stringWithFormat:@"平台：%@\n账号：%@\n密码：%@", dic[PlatformStr], dic[AccountStr], dic[PwdStr]]];
                        __strong typeof(weakSelf) strongSelf = self;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [strongSelf refreshStateLabel:@"密码已放入剪切板"];
                            [strongSelf clearAllTextFieldContent];
                        });
                    }
                }];
            } failBlock:^(NSString * _Nonnull message) {
                __strong typeof(weakSelf) strongSelf = self;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [strongSelf refreshStateLabel:@"查询失败！请检查输入！"];
                    [strongSelf clearAllTextFieldContent];
                });
            }];
        }
    } else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"⚠️安全提示" message:@"请重新进行指纹验证，取消将无法新建密码！" preferredStyle:(UIAlertControllerStyleAlert)];
        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf checkLocalAuthentication:^(BOOL isSuccess) {
                
            }];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [self presentViewController:alertController animated:YES completion:^{
            
        }];
    }
}

- (IBAction)allPwdBtnAction:(UIButton *)sender {
    AllPwdListViewController *allPwdListViewController = [[AllPwdListViewController alloc] initWithNibName:@"AllPwdListViewController" bundle:[NSBundle mainBundle]];
    allPwdListViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:allPwdListViewController animated:YES completion:nil];
}

- (BOOL)canEvaluatePolicy:(LAPolicy)policy error:(NSError * __autoreleasing *)error {
    return YES;
}

-(void)showViews {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [UIView animateWithDuration:0.5 animations:^{
            NSArray *views = strongSelf.backView.subviews;
            for (int i = 0; i < views.count; i++) {
                UIView *view = views[i];
                view.alpha = 1.0;
            }
            if(!strongSelf.inOrOutSwitch.isOn) {
                strongSelf.pwdTextField.alpha = 0;
                strongSelf.pwdLengthBtn.alpha = 0;
                strongSelf.createPwdBtn.alpha = 0;
                strongSelf.searchStateLabel.alpha = 1.0;
            } else {
                strongSelf.searchStateLabel.alpha = 0;
            }
            strongSelf.isLoacalAuthentication = YES;
        }];
    });
}

-(void)hideViews {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [UIView animateWithDuration:0.5 animations:^{
            NSArray *views = strongSelf.backView.subviews;
            for (int i = 0; i < views.count; i++) {
                UIView *view = views[i];
                view.alpha = 0;
            }
            strongSelf.isLoacalAuthentication = NO;
        }];
    });
}

-(void)refreshStateLabel:(NSString *)text {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
//        weakSelf.stateLabel.alpha = 1.0;
        weakSelf.stateLabel.text = text;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            weakSelf.stateLabel.alpha = 0;
            weakSelf.stateLabel.text = @"";
        });
    });
}

-(void)clearAllTextFieldContent {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.platformTextField.text = @"";
        weakSelf.accountTextField.text = @"";
        weakSelf.pwdTextField.text = @"";
    });
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

-(NSMutableArray *)keysNameArray {
    if(!_keysNameArray) {
        _keysNameArray = [NSMutableArray new];
    }
    return _keysNameArray;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.isLoacalAuthentication) {
        return self.keysNameArray.count>8?8:self.keysNameArray.count;
    } else {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:@"cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *arr = [self.keysNameArray[indexPath.row][keysName] componentsSeparatedByString:@"-"];
    if(arr && arr.count == 2) {
        cell.textLabel.text = arr.lastObject;
        cell.detailTextLabel.text = arr.firstObject;
    }
    cell.textLabel.font = [UIFont systemFontOfSize:13];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:11];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) weakSelf = self;
    NSArray *keysNameArr = [self.keysNameArray[indexPath.row][keysName] componentsSeparatedByString:@"-"];
    if(keysNameArr.count == 2) {
        [[PPT_Manager sharedInstance] getPwdWithAccount:keysNameArr.firstObject platform:keysNameArr.lastObject successBlock:^(NSDictionary * _Nullable dic) {
            __strong typeof(weakSelf) strongSelf = self;
            [weakSelf checkLocalAuthentication:^(BOOL isSuccess) {
                if(isSuccess) {
                    [[UIPasteboard generalPasteboard] setString:[NSString stringWithFormat:@"平台：%@\n账号：%@\n密码：%@", dic[PlatformStr], dic[AccountStr], dic[PwdStr]]];
                    __weak typeof(strongSelf) weakStrongSelf = strongSelf;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakStrongSelf refreshStateLabel:@"密码已放入剪切板"];
                    });
                }
            }];
        } failBlock:^(NSString * _Nonnull message) {
            [weakSelf refreshStateLabel:message];
        }];
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(editingStyle == UITableViewCellEditingStyleDelete) {
        __weak typeof(self) weakSelf = self;
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"即将删除密码" message:@"删除后不可恢复，是否确认立即删除？" preferredStyle:(UIAlertControllerStyleAlert)];
        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [weakSelf checkLocalAuthentication:^(BOOL isSuccess) {
                __weak typeof(strongSelf) weakStrongSelf = strongSelf;
                if(isSuccess) {
                    [[PPT_Manager sharedInstance] deletePwdWithName:strongSelf.keysNameArray[indexPath.row][keysName] isSuccessBlock:^(BOOL isSuccess, NSString * _Nullable msg) {
                        [weakStrongSelf refreshKeysTableView];
                    }];
                } else {
                    [strongSelf refreshStateLabel:@"验证失败，无权限删除密码！"];
                }
            }];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf refreshKeysTableView];
        }]];
        [self presentViewController:alertController animated:YES completion:^{
            
        }];
    }
}

@end
