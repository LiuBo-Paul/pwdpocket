//
//  ViewController.h
//  PwdPocket
//
//  Created by Paul on 2019/5/13.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import "BaseViewController.h"
#import "PPT_ManagerHeader.h"
#import "PPT_LocalAuthenticationManager.h"
#import "AllPwdListViewController.h"
#import "AboutPwdPocketViewController.h"
#import "SettingPwdPocketViewController.h"

@interface ViewController : BaseViewController

-(void)checkLocalAuthentication:(LocalAuthenticationManagerIsSuccessBlock)isSuccessBlock;

@end

