//
//  BaseViewController.h
//  PwdPocket
//
//  Created by Paul on 2019/6/19.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
