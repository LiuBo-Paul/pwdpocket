//
//  AllPwdListViewController.m
//  PwdPocket
//
//  Created by Paul on 2019/5/15.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import "AllPwdListViewController.h"
#import "PPT_Manager.h"
#import "PPT_LocalAuthenticationManager.h"

@interface AllPwdListViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (strong, nonatomic) IBOutlet UILabel *stateLabel;

@end

@implementation AllPwdListViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self refreshStateLabel:@"加载完成！"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self refreshTableView];
}

-(void)refreshTableView {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.dataArray = [NSMutableArray arrayWithArray:[[PPT_Manager sharedInstance] getKeysFromKeychain]];
        [weakSelf.tableView reloadData];
    });
}

- (IBAction)backBtnAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)clearListBtnAction:(UIButton *)sender {
    __weak typeof(self) weakSelf = self;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"即将删除所有密码" message:@"删除后不可恢复，是否确认立即删除？" preferredStyle:(UIAlertControllerStyleAlert)];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [[PPT_LocalAuthenticationManager sharedInstance] checkLocalAuthenticationStateIsSuccess:^(BOOL isSuccess) {
            if(isSuccess) {
                [[PPT_Manager sharedInstance] rebootPwd];
                [strongSelf refreshTableView];
            }
        }];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf refreshTableView];
    }]];
    [self presentViewController:alertController animated:YES completion:^{
        
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:@"cell"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *arr = [self.dataArray[indexPath.row][keysName] componentsSeparatedByString:@"-"];
    if(arr && arr.count == 2) {
        cell.textLabel.text = arr.lastObject;
        cell.detailTextLabel.text = arr.firstObject;
    }
    cell.textLabel.font = [UIFont systemFontOfSize:13];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:11];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 添加一个删除按钮
    __weak typeof(self) weakSelf = self;
    UITableViewRowAction *deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除"handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"即将删除密码" message:@"删除后不可恢复，是否确认立即删除？" preferredStyle:(UIAlertControllerStyleAlert)];
        [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            __weak typeof(strongSelf) weakStrongSelf = strongSelf;
            [[PPT_LocalAuthenticationManager sharedInstance] checkLocalAuthenticationStateIsSuccess:^(BOOL isSuccess) {
                __strong typeof(weakStrongSelf) strongWeakStrongSelf = weakStrongSelf;
                if(isSuccess) {
                    [[PPT_Manager sharedInstance] deletePwdWithName:weakStrongSelf.dataArray[indexPath.row][keysName] isSuccessBlock:^(BOOL isSuccess, NSString * _Nullable msg) {
                        [strongWeakStrongSelf refreshTableView];
                    }];
                } else {
                    [weakStrongSelf refreshStateLabel:@"验证失败，无权限删除密码！"];
                }
            }];
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf refreshTableView];
        }]];
        [weakSelf presentViewController:alertController animated:YES completion:^{
            
        }];
    }];
    // 将设置好的按钮放到数组中返回
    return @[deleteRowAction];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) weakSelf = self;
    NSArray *keysNameArr = [self.dataArray[indexPath.row][keysName] componentsSeparatedByString:@"-"];
    if(keysNameArr.count == 2) {
        [[PPT_Manager sharedInstance] getPwdWithAccount:keysNameArr.firstObject platform:keysNameArr.lastObject successBlock:^(NSDictionary * _Nullable dic) {
            __strong typeof(weakSelf) strongSelf = self;
            [[PPT_LocalAuthenticationManager sharedInstance] checkLocalAuthenticationStateIsSuccess:^(BOOL isSuccess) {
                if(isSuccess) {
                    [[UIPasteboard generalPasteboard] setString:[NSString stringWithFormat:@"平台：%@\n账号：%@\n密码：%@", dic[PlatformStr], dic[AccountStr], dic[PwdStr]]];
                    __weak typeof(strongSelf) weakStrongSelf = strongSelf;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakStrongSelf refreshStateLabel:@"密码已放入剪切板"];
                    });
                }
            }];
        } failBlock:^(NSString * _Nonnull message) {
            [weakSelf refreshStateLabel:message];
        }];
    }
}

-(NSMutableArray *)dataArray {
    if(!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

-(void)refreshStateLabel:(NSString *)text {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        weakSelf.stateLabel.alpha = 1.0;
        weakSelf.stateLabel.text = text;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            weakSelf.stateLabel.alpha = 0;
        });
    });
}

@end
