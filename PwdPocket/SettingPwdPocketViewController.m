//
//  SettingPwdPocketViewController.m
//  PwdPocket
//
//  Created by Paul on 2019/6/19.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import "SettingPwdPocketViewController.h"

@interface SettingPwdPocketViewController ()

@property (strong, nonatomic) IBOutlet UILabel *warningLabel;

@end

static NSString *ProtocolUrlStr = @"http://18.221.81.60/ProtocolPwdPocket.txt";
static NSString *ProveUrlStr = @"https://www.cnblogs.com/PaulpauL/";
static NSString *FounctionUrlStr = @"http://18.221.81.60/FounctionPwdPocket.txt";
static NSString *AppId = @"1469018173";

@implementation SettingPwdPocketViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.warningLabel.text = @"此页面部分功能需要联网才能使用，请保持网络畅通！";
}

- (IBAction)backBtnAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)itemsBtnAction:(UIButton *)sender {
    [UIView animateWithDuration:0.8 animations:^{
        sender.alpha = 0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 animations:^{
            sender.alpha = 1;
        }];
    }];
    NSUInteger tag = sender.tag - 600;
    switch (tag) {
        case 0:{
            // 隐私协议
            [self showMsgWithUrl:ProtocolUrlStr title:@"隐私协议"];
        } break;
        case 1:{
            // 支持网址
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ProveUrlStr] options:@{} completionHandler:nil];
        } break;
        case 3:{
            // 功能介绍
            [self showMsgWithUrl:FounctionUrlStr title:@"功能介绍"];
        } break;
        case 5:{
            // 技术交流
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ProveUrlStr] options:@{} completionHandler:nil];
        } break;
        case 7:{
            // 版本号
            [sender setTitle:[NSString stringWithFormat:@"V %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]] forState:(UIControlStateNormal)];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [sender setTitle:@"版本号" forState:(UIControlStateNormal)];
            });
        } break;
        case 8:{
            // 意见反馈
            NSString *itunesurl = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/cn/app/id%@?mt=8&action=write-review", AppId];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:itunesurl] options:@{} completionHandler:nil];
        } break;
        default:{
            
        } break;
    }
}

-(void)showMsgWithUrl:(NSString *)urlStr title:(NSString *)title {
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *msg =[NSString stringWithContentsOfURL:[NSURL URLWithString:urlStr] encoding:(NSUTF8StringEncoding) error:nil];
            if(msg.length == 0) {
                msg = @"暂无内容";
            }
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:(UIAlertControllerStyleAlert)];
            [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            [strongSelf presentViewController:alertController animated:YES completion:^{
                
            }];
        });
    });
}

@end
