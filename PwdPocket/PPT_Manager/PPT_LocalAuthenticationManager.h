//
//  PPT_LocalAuthenticationManager.h
//  PwdPocket
//
//  Created by Paul on 2019/5/15.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <LocalAuthentication/LocalAuthentication.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^LocalAuthenticationManagerIsSuccessBlock)(BOOL isSuccess);

@interface PPT_LocalAuthenticationManager : NSObject

+(instancetype)sharedInstance;

-(void)checkLocalAuthenticationStateIsSuccess:(LocalAuthenticationManagerIsSuccessBlock)isSuccess;

@end

NS_ASSUME_NONNULL_END
