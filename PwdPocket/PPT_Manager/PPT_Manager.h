//
//  PPT_Manager.h
//  PwdPocket
//
//  Created by Paul on 2019/5/13.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FSCacheManager.h"

NS_ASSUME_NONNULL_BEGIN

static NSString *AccountStr = @"AccountString";
static NSString *PwdStr = @"PwdString";
static NSString *PlatformStr = @"PlatformString";
static NSString *OperateTime = @"OperateTime";
static NSString *ArchiverKey = @"ArchiverKeyString";
static NSString *keysName = @"keysNameString";

@interface PPT_Manager : NSObject

typedef enum : NSUInteger {
    PPT_PwdLevelDefault,
    PPT_PwdLevelHigh,
    PPT_PwdLevelLow,
} PPT_PwdLevel;

typedef void(^PPT_ManagerDidSuccess)(NSDictionary * _Nullable dic);
typedef void(^PPT_ManagerDidFail)(NSString * _Nonnull message);
typedef void(^PPT_ManagerIsSuccess)(BOOL isSuccess, NSString * _Nullable msg);

+(instancetype)sharedInstance;

/**
 恢复密码初始状态
 */
-(void)rebootPwd;

/**
 新建密码

 @param account 账户名
 @param pwd 密码
 @param platform 平台名
 @param isSuccessBlock 是否成功回调
 */
-(void)createPwdWithAccount:(NSString *)account pwd:(NSString *)pwd platform:(NSString *)platform isSuccessBlock:(PPT_ManagerIsSuccess)isSuccessBlock;

/**
 刷新密码

 @param account 账户名
 @param pwd 密码
 @param platform 平台名
 @param isSuccessBlock 是否成功回调
 */
-(void)refreshPwdWithAccount:(NSString *)account pwd:(NSString *)pwd platform:(NSString *)platform isSuccessBlock:(PPT_ManagerIsSuccess)isSuccessBlock;

/**
 删除密码
 
 @param name 密钥名（账户名-平台名）
 @param isSuccessBlock 是否成功回调
 */
-(void)deletePwdWithName:(NSString *)name isSuccessBlock:(PPT_ManagerIsSuccess)isSuccessBlock;

/**
 读取密码

 @param account 账户名
 @param platform 平台名
 @param successBlock 成功回调
 @param failBlock 失败回调
 */
-(void)getPwdWithAccount:(NSString *)account platform:(NSString *)platform successBlock:(PPT_ManagerDidSuccess)successBlock failBlock:(PPT_ManagerDidFail)failBlock;

/**
 读取钥匙串中所有已存储的密码并写入缓存
 */
-(void)readKeysFromKeychain;

/**
 读取钥匙串中所有已存储的密码
 */
-(NSArray *)getKeysFromKeychain;

/**
 生成密码

 @param level 密码等级
 @param length 密码长度
 @return 密码
 */
-(NSString *)createPwdWithLevel:(PPT_PwdLevel)level length:(NSUInteger)length;

/**
 字典转data

 @param dic 字典
 @return data
 */
-(NSData *)returnDataWithDictionary:(NSDictionary*)dic;

/**
 data转字典

 @param data data
 @return 字典
 */
-(NSDictionary *)returnDictionaryWithData:(NSData*)data;

/**
 数组转data

 @param arr h数组
 @return data
 */
-(NSData *)returnDataWithArray:(NSArray *)arr;

/**
 data转数组

 @param data data
 @return 数组
 */
-(NSArray *)returnArrayWithData:(NSData*)data;

@end

NS_ASSUME_NONNULL_END
