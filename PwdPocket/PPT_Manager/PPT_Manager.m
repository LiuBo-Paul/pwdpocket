//
//  PPT_Manager.m
//  PwdPocket
//
//  Created by Paul on 2019/5/13.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import "PPT_Manager.h"
#import "KeyChainStore.h"

@implementation PPT_Manager

static PPT_Manager *manager = nil;
static NSString *allNamesKey = @"AllNamesKeyString";
static NSString *asciiKey = @"asciiKeyString";

+(instancetype)sharedInstance {
    if(manager == nil) {
        manager = [PPT_Manager new];
    }
    return manager;
}

-(void)rebootPwd {
    [[FSCacheManager sharedManager] clearAllCache];
    NSMutableArray *arr = [NSMutableArray arrayWithArray:[self returnArrayWithData:[KeyChainStore load:allNamesKey]]];
    for (int i = 0; i < arr.count; i++) {
        [KeyChainStore deleteKeyData:arr[i][keysName]];
    }
    [KeyChainStore deleteKeyData:allNamesKey];
}

-(void)createPwdWithAccount:(NSString *)account pwd:(NSString *)pwd platform:(NSString *)platform isSuccessBlock:(PPT_ManagerIsSuccess)isSuccessBlock {
    NSString *name = [NSString stringWithFormat:@"%@-%@", account, platform];
    [[FSCacheManager sharedManager] saveData:[self returnDataWithDictionary:[self integrationAccount:account pwd:pwd platform:platform]] name:name cacheType:(FSCacheTypeGeneral) successBlock:^(NSString * _Nullable message, NSData * _Nullable data, NSString * _Nullable path) {
        
        // 存储所有密码的关键字
        NSMutableArray *arr = [NSMutableArray arrayWithArray:[self returnArrayWithData:[KeyChainStore load:allNamesKey]]];
        NSMutableArray *mArr = [NSMutableArray new];
        [mArr addObject:@{keysName:name}];
        if(arr) {
            // 数组去重
            for (int i = 0; i < arr.count; i++) {
                NSDictionary *subDic = arr[i];
                BOOL isExist = NO;
                for (int j = 0; j < mArr.count; j++) {
                    if([subDic[keysName] isEqualToString:mArr[j][keysName]]) {
                        isExist = YES;
                        break;
                    }
                }
                if(!isExist) {
                    [mArr addObject:arr[i]];
                }
            }
        }
        [KeyChainStore save:allNamesKey data:[self returnDataWithArray:mArr]];
        
        // 把密码存入钥匙串
        while ([KeyChainStore load:name]) {
            [KeyChainStore deleteKeyData:name];
        }
        [KeyChainStore save:name data:data];
        
        isSuccessBlock(YES, @"Success!");
        
    } failBlock:^(NSString * _Nonnull message) {
        isSuccessBlock(NO, message);
    }];
}

-(void)refreshPwdWithAccount:(NSString *)account pwd:(NSString *)pwd platform:(NSString *)platform isSuccessBlock:(PPT_ManagerIsSuccess)isSuccessBlock {
    [[FSCacheManager sharedManager] clearCacheWithName:[NSString stringWithFormat:@"%@-%@", account, platform] cacheType:(FSCacheTypeGeneral)];
    [[FSCacheManager sharedManager] saveData:[self returnDataWithDictionary:[self integrationAccount:account pwd:pwd platform:platform]] name:[NSString stringWithFormat:@"%@-%@", account, platform] cacheType:(FSCacheTypeGeneral) successBlock:^(NSString * _Nullable message, NSData * _Nullable data, NSString * _Nullable path) {
        isSuccessBlock(YES, @"Success!");
    } failBlock:^(NSString * _Nonnull message) {
        isSuccessBlock(NO, message);
    }];
}

-(void)deletePwdWithName:(NSString *)name isSuccessBlock:(PPT_ManagerIsSuccess)isSuccessBlock {
    [[FSCacheManager sharedManager] clearCacheWithName:name cacheType:(FSCacheTypeGeneral)];
    NSMutableArray *arr = [NSMutableArray arrayWithArray:[self returnArrayWithData:[KeyChainStore load:allNamesKey]]];
    for (int i = 0; i < arr.count; i++) {
        NSDictionary *dic = arr[i];
        if([dic[keysName] isEqual:name]) {
            [arr removeObjectAtIndex:i];
            break;
        }
    }
    [KeyChainStore save:allNamesKey data:[self returnDataWithArray:arr]];
    isSuccessBlock(YES, @"Success!");
}

-(void)getPwdWithAccount:(NSString *)account platform:(NSString *)platform successBlock:(PPT_ManagerDidSuccess)successBlock failBlock:(PPT_ManagerDidFail)failBlock {
    __weak typeof(self) weakSelf = self;
    [[FSCacheManager sharedManager] searchDataWithName:[NSString stringWithFormat:@"%@-%@", account, platform] cacheType:(FSCacheTypeGeneral) successBlock:^(NSString * _Nullable message, NSData * _Nullable data, NSString * _Nullable path) {
        successBlock([weakSelf returnDictionaryWithData:data]);
    } failBlock:^(NSString * _Nonnull message) {
        failBlock(message);
    }];
}

-(void)readKeysFromKeychain {
    __weak typeof(self) weakSelf = self;
    NSArray *arr = [self getKeysFromKeychain];
    if(arr && arr.count > 0) {
        for (int i = 0; i < arr.count; i++) {
            NSDictionary *dic = arr[i];
            if([[FSCacheManager sharedManager] isExistFileWithFileName:dic[keysName]]) {
                // 缓存文件已经存在
            } else {
                NSDictionary *keysNameDic = [self returnDictionaryWithData:[KeyChainStore load:dic[keysName]]];
                [weakSelf createPwdWithAccount:keysNameDic[AccountStr] pwd:keysNameDic[PwdStr] platform:keysNameDic[PlatformStr] isSuccessBlock:^(BOOL isSuccess, NSString * _Nullable msg) {
                    
                }];
            }
        }
    }
}

-(NSArray *)getKeysFromKeychain {
    NSData *allNamesKeyData = [KeyChainStore load:allNamesKey];
    return [self returnArrayWithData:allNamesKeyData];
}

-(NSString *)createPwdWithLevel:(PPT_PwdLevel)level length:(NSUInteger)length {
    NSMutableString *pwdString = [NSMutableString new];
    NSString *str = @"0;NUT;32;(space);64;@;96;、;1;SOH;33;!;65;A;97;a;2;STX;34;\";66;B;98;b;3;ETX;35;#;67;C;99;c;4;EOT;36;$;68;D;100;d;5;ENQ;37;%;69;E;101;e;6;ACK;38;&;70;F;102;f;7;BEL;39;,;71;G;103;g;8;BS;40;(;72;H;104;h;9;HT;41;);73;I;105;i;10;LF;42;*;74;J;106;j;11;VT;43;+;75;K;107;k;12;FF;44;,;76;L;108;l;13;CR;45;-;77;M;109;m;14;SO;46;.;78;N;110;n;15;SI;47;/;79;O;111;o;16;DLE;48;0;80;P;112;p;17;DCI;49;1;81;Q;113;q;18;DC2;50;2;82;R;114;r;19;DC3;51;3;83;S;115;s;20;DC4;52;4;84;T;116;t;21;NAK;53;5;85;U;117;u;22;SYN;54;6;86;V;118;v;23;TB;55;7;87;W;119;w;24;CAN;56;8;88;X;120;x;25;EM;57;9;89;Y;121;y;26;SUB;58;:;90;Z;122;z;27;ESC;59;59;91;[;123;{;28;FS;60;<;92;/;124;|;29;GS;61;=;93;];125;};30;RS;62;>;94;^;126;`;31;US;63;?;95;_;127;DEL";
    NSArray *arr = [str componentsSeparatedByString:@";"];
    NSMutableDictionary *mDic = [NSMutableDictionary new];
    for (int i = 0; i+1 < arr.count; i+=2) {
        if([arr[i] isEqualToString:@"59"]) {
            [mDic setObject:@";" forKey:arr[i]];
        } else {
            [mDic setObject:arr[i+1] forKey:arr[i]];
        }
    }
    NSMutableString *levelString = [NSMutableString new];
    //数字
    for (int i = 48; i <= 57; i++) {
        [levelString appendString:mDic[[NSString stringWithFormat:@"%ld", (long)i]]];
    }
    switch (level) {
        case PPT_PwdLevelDefault: {
            //小写字母
            for (int i = 97; i <= 122; i++) {
                [levelString appendString:mDic[[NSString stringWithFormat:@"%ld", (long)i]]];
            }
            //大写字母
            for (int i = 65; i <= 90; i++) {
                [levelString appendString:mDic[[NSString stringWithFormat:@"%ld", (long)i]]];
            }
        } break;
        case PPT_PwdLevelHigh: {
            //小写字母
            for (int i = 97; i <= 122; i++) {
                [levelString appendString:mDic[[NSString stringWithFormat:@"%ld", (long)i]]];
            }
            //大写字母
            for (int i = 65; i <= 90; i++) {
                [levelString appendString:mDic[[NSString stringWithFormat:@"%ld", (long)i]]];
            }
            //特殊字符
            for (int i = 33; i <= 47; i++) {
                [levelString appendString:mDic[[NSString stringWithFormat:@"%ld", (long)i]]];
            }
            for (int i = 58; i <= 64; i++) {
                [levelString appendString:mDic[[NSString stringWithFormat:@"%ld", (long)i]]];
            }
            for (int i = 91; i <= 96; i++) {
                [levelString appendString:mDic[[NSString stringWithFormat:@"%ld", (long)i]]];
            }
            for (int i = 123; i <= 126; i++) {
                [levelString appendString:mDic[[NSString stringWithFormat:@"%ld", (long)i]]];
            }
        } break;
        case PPT_PwdLevelLow: {
            //小写字母
            for (int i = 97; i <= 122; i++) {
                [levelString appendString:mDic[[NSString stringWithFormat:@"%ld", (long)i]]];
            }
        } break;
        default:
            break;
    }
    for (int i = 0; i < length; i++) {
        [pwdString appendString:[levelString substringWithRange:NSMakeRange(arc4random()%levelString.length, 1)]];
    }
    NSLog(@"%@", [pwdString copy]);
    return [pwdString copy];
}

-(NSDictionary *)integrationAccount:(NSString *)account pwd:(NSString *)pwd platform:(NSString *)platform {
    return @{AccountStr:account, PwdStr:pwd, PlatformStr:platform, OperateTime:@([[NSDate date] timeIntervalSinceNow])};
}

//NSDictonary转NSData
-(NSData *)returnDataWithDictionary:(NSDictionary*)dic {
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:dic forKey:ArchiverKey];
    [archiver finishEncoding];
    return data;
}

// NSData转NSDictonary
-(NSDictionary *)returnDictionaryWithData:(NSData*)data {
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    NSDictionary *dic = [unarchiver decodeObjectForKey:ArchiverKey];
    [unarchiver finishDecoding];
    return dic;
}

//NSDictonary转NSData
-(NSData *)returnDataWithArray:(NSArray *)arr {
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:arr forKey:ArchiverKey];
    [archiver finishEncoding];
    return data;
}

// NSData转NSDictonary
-(NSArray *)returnArrayWithData:(NSData*)data {
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    NSArray *arr = [unarchiver decodeObjectForKey:ArchiverKey];
    [unarchiver finishDecoding];
    return arr;
}

@end
