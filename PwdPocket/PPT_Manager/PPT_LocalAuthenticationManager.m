//
//  PPT_LocalAuthenticationManager.m
//  PwdPocket
//
//  Created by Paul on 2019/5/15.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import "PPT_LocalAuthenticationManager.h"

static PPT_LocalAuthenticationManager *manager;
@implementation PPT_LocalAuthenticationManager

+(instancetype)sharedInstance {
    if(manager == nil) {
        manager = [PPT_LocalAuthenticationManager new];
    }
    return manager;
}

-(void)checkLocalAuthenticationStateIsSuccess:(LocalAuthenticationManagerIsSuccessBlock)isSuccess {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        //首先判断版本
        if (NSFoundationVersionNumber < NSFoundationVersionNumber_iOS_8_0) {
            NSLog(@"系统版本不支持TouchID");
            return;
        }
        LAContext *context = [[LAContext alloc] init];
        context.localizedCancelTitle = @"取消"; // 自定义 左边 title
        context.localizedFallbackTitle = @"手动输入密码";
        NSError *error;
        if([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthentication error:&error]){
            [context evaluatePolicy:LAPolicyDeviceOwnerAuthentication localizedReason:@"指纹安全验证..." reply:^(BOOL success, NSError * _Nullable error) {
                if(success) {
                    NSLog(@"验证 Success");
                    isSuccess(YES);
                } else {
                    isSuccess(NO);
                    switch (error.code) {
                        case LAErrorUserCancel: {
                            //认证被用户取消.例如点击了 cancel 按钮.
                            [weakSelf alertMessageWithTitle:@"验证失败" message:@"指纹验证已取消！"];
                        } break;
                        case LAErrorAuthenticationFailed: {
                            // 此处会自动消失，然后下一次弹出的时候，又需要验证数字
                            // 认证没有成功,因为用户没有成功的提供一个有效的认证资格
                            [weakSelf alertMessageWithTitle:@"验证失败" message:@"连输三次后，密码失败"];
                        } break;
                        case LAErrorPasscodeNotSet: {
                            // 认证不能开始,因为此台设备没有设置密码.
                            [weakSelf alertMessageWithTitle:@"验证失败" message:@"密码没有设置，请先为您的设备设置密码"];
                        } break;
                        case LAErrorSystemCancel: {
                            //认证被系统取消了(例如其他的应用程序到前台了)
                            [weakSelf alertMessageWithTitle:@"验证失败" message:@"系统取消了验证"];
                        } break;
                        case LAErrorUserFallback: {
                            //用户不想进行Touch ID验证，想进行输入密码操作
                            [weakSelf alertMessageWithTitle:@"验证失败" message:@"指纹验证已取消，即将进行输入密码操作"];
                        } break;
                        case LAErrorTouchIDNotAvailable: {
                            //认证不能开始,因为 touch id 在此台设备尚是无效的.
                            [weakSelf alertMessageWithTitle:@"验证失败" message:@"touch ID 无效"];
                        } break;
                        default: {
                            [weakSelf alertMessageWithTitle:@"验证失败" message:@"原因未知"];
                        } break;
                    }
                }
            }];
        } else {
            isSuccess(NO);
            switch (error.code) {
                case LAErrorTouchIDNotEnrolled: {
                    [weakSelf alertMessageWithTitle:@"验证失败" message:@"您还没有进行指纹输入，请指纹设定后打开"];
                } break;
                case  LAErrorTouchIDNotAvailable: {
                    [weakSelf alertMessageWithTitle:@"验证失败" message:@"您的设备不支持指纹输入，请切换为数字键盘"];
                } break;
                case LAErrorPasscodeNotSet: {
                    [weakSelf alertMessageWithTitle:@"验证失败" message:@"您还没有设置密码输入"];
                } break;
                default: {
                    [weakSelf alertMessageWithTitle:@"验证失败" message:@"原因未知"];
                } break;
            }
        }
    });
}

-(void)alertMessageWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:(UIAlertControllerStyleAlert)];
    [alertController addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:^{
        
    }];
}

@end
