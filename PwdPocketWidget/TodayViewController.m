//
//  TodayViewController.m
//  PwdPocketWidget
//
//  Created by Paul on 2019/6/24.
//  Copyright © 2019 LiuBo. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>

@interface TodayViewController () <NCWidgetProviding>

@property (strong, nonatomic) IBOutlet UILabel *mainLabel;

@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //不设置,iOS10,可能不会出现展开/折叠按钮
    self.extensionContext.widgetLargestAvailableDisplayMode = NCWidgetDisplayModeExpanded;

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    self.mainLabel.text = [NSString stringWithFormat:@"今天是%@，你好啊！", [formatter stringFromDate:[NSDate date]]];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 9.9
        && [[[UIDevice currentDevice] systemVersion] floatValue] > 8.9) {
        [self widgetPerformUpdateWithCompletionHandler:^(NCUpdateResult result) {}];
    }
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

//使用open Url拉起方式
- (void)openContainingAPPWithURL:(NSString *)urlStr{
    //通过extensionContext借助host app调起app
    NSURL *url = [NSURL URLWithString:urlStr.length>0?[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]:@"PwdPocketWidget://"];
    [self.extensionContext openURL:url completionHandler:nil];
}

@end
